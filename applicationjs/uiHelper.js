

function ProcessDomainList(response) {
    $('#domainAuction').hide();
    $('#domainBid').hide();
    $('#domainList').show();
    var trHTML = '';
    $.each(response, function (i, item) {
        trHTML += '<tr><td>' + item.domain_name + '</td><td>' + item.ownerId + '</td><td>' + item.auction + '</td></tr>';
    });
    $('#domainList').empty();
    $('#domainList').append(trHTML);

}

function ProcessBidList(response) {
    $('#domainList').hide();
    $('#domainAuction').hide();
    $('#domainBid').show();
    var trHTML = '';
    $.each(response, function (i, item) {
        trHTML += '<tr><td>' + item.domainName + '</td><td>' + item.userId + '</td><td>' + item.bidAmount + '</td></tr>';
    });
    $('#domainBid').empty();
    $('#domainBid').append(trHTML);

}

function ProcessAuctionList(response) {
    $('#domainBid').hide();
    $('#domainList').hide();
    $('#domainAuction').show();
    var trHTML = '';
    $.each(response, function (i, item) {
        trHTML += '<tr><td>' + item.domain_name + '</td><td>' + item.ownerId + '</td><td>' + item.auction + '</td></tr>';
    });
    $('#domainAuction').empty();
    $('#domainAuction').append(trHTML);
}

function ProcessSearchList(response) {
    //$('#searchResult').show();
    
    var trHTML = '<li class="collection-header"><strong>Results</strong></li>';
    $.each(response, function (i, item) {
        trHTML += '<li class="collection-item"> <p class="orange-text"><strong>' + item.domain_name + '</strong><br/><span class="black-text">Owner ' + item.ownerId + '</span></p></li>';
    });

   
    $('#searchResult').empty();
    $('#searchResult').append(trHTML);

}




