
function iaInit(key) {
    this.apikey = key;
    console.log('IA Initialized');

    this.get = function (url, callback) {
        console.log("GET request called.");
        processGet(url, key, function (data) {
            callback(data);
        });
       
    }
    this.delete = function (url,param) {
    }

    this.post = function (url, param) {
    }

    this.put = function (url, param) {
    }
}

function processGet(endAddress, apikey, callback) {
    //request
    //    .get(endAddress)
    //  .set('Content-Type', 'application/json')
    //  .set('apikey', apikey)
    //  .end(callback);


    $.ajax({
        url: endAddress,
        type: 'GET',
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('apikey', apikey);
            xhr.setRequestHeader('Content-Type', 'application/json');
        }
    }).done(function (data) {
        callback(data);
    }).fail(function (jqXhr, textStatus, errorThrown) {
        callback(null);
    });
}




